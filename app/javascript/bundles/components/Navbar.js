import React, { Component } from "react";
// import { a } from "react-router-dom";

class Navbar extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     search: ""
  //   };
  // }

  // handleChange = e => {
  //   this.setState({ search: e.target.value });
  // };

  // onFormSubmit = e => {
  //   e.preventDefault();
  //   // console.log(this.state.search);
  //   // console.log(this.props.history)
  //   this.setState({ search: "" });
  //   this.props.history.push(`/search?params=${this.state.search}`);
  // };

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-primary mt-2">
          <a href="/" className="navbar-brand text-white">
            Lunch&Learn Manager
          </a>

          <form className="form-inline my-2 ml-5 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="search"
              name="search"
              // onChange={this.handleChange}
              placeholder="Search Talk"
              aria-label="Search"
              // value={this.state.search}
            />
            <button
              // onClick={this.onFormSubmit}
              className="btn btn-success my-2 my-sm-0"
            >
              Search
            </button>
          </form>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <a href="/" className="nav-a text-white">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a href="/talks/schedule" className="nav-a text-white">
                  Schedule a Talk
                </a>
              </li>
              <li className="nav-item">
                <a href="/talks/upcoming" className="nav-a text-white">
                  Upcoming Talks
                </a>
              </li>
              <li className="nav-item">
                <a href="/talks/recent" className="nav-a text-white">
                  Recent Talks
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
